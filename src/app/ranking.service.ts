import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ranking } from './ranking';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  rankingUrl = 'http://localhost:8080/ranking/'

  constructor(private http:HttpClient ) {

   }

  listar(){
    return this.http.get<Ranking[]>(`${this.rankingUrl}`);
  }

}
