import { Component, OnInit } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";
import { Jogador } from "../jogador";
import { Ranking } from "../ranking";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { error } from "util";
import { RankingSelectComponent } from "../ranking-select/ranking-select.component";
import { RankingService } from "../ranking.service";

@Component({
  selector: "ranking-form",
  templateUrl: "./ranking-form.component.html",
  styleUrls: ["./ranking-form.component.css"]
})
export class RankingFormComponent implements OnInit {
  RankingUrlBase = "http://localhost:8080/ranking/";

  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };

  private jogador: Jogador;
  private rank: Ranking;
  private newJogador: Observable<Jogador>;
  firstName: string;
  vitorias: number;
  partidas: number;
  updatable: boolean;

  ranking: Array<Ranking>;
  rankSelected: number;
  rankSelectModified: String;
  rankSelectedId: number;

  constructor(
    private http: HttpClient,
    private rankingService: RankingService
  ) {
    this.updatable = false;
  }

  ngOnInit() {
    this.listar();
  }

  post(nome, vitorias, partidas) {

    if (this.updatable === true) {

      this.rank.partidas = partidas;
      this.rank.vitorias = vitorias;

      this.http
        .put<Ranking>(
          this.RankingUrlBase + this.rank.id,
          this.rank,
          this.httpOptions
        )
        .subscribe(
          result => {
             window.location.reload();
          },
          error => console.error(error)
        );

    } else {
      this.jogador = new Jogador();
      this.jogador.nome = nome;
      this.jogador.id = 0;
      this.rank = new Ranking(this.jogador, vitorias, partidas);

      this.http
        .post<Ranking>(this.RankingUrlBase, this.rank, this.httpOptions)
        .subscribe(
          result => {
            window.location.reload();
          },
          error => console.error(error)
        );
    }
  }


  listar() {
    this.rankingService.listar().subscribe(dados => {
      this.ranking = dados;
      this.rankSelected = 1;
    });
  }

  onRankSelected(val: any) {
    this.http.get<Ranking>(this.RankingUrlBase + val).subscribe(
      result => {

        this.rank = result;
        this.firstName = result.jogador.nome;
        this.vitorias = result.vitorias;
        this.partidas = result.partidas;
        this.updatable = true;

      },
      error => console.error(error)
    );
  }
}
