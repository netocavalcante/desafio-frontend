import { Jogador } from './jogador';

export class Ranking {
  id:number
  jogador:Jogador
  vitorias:number
  partidas:number

  constructor(jogador:Jogador, vitorias:number, partidas:number ){
    this.jogador = jogador;
    this.vitorias = vitorias;
    this.partidas = partidas;
  }
}
