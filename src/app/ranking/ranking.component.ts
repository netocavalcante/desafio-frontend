import { Component, OnInit } from '@angular/core';
import { RankingService } from '../ranking.service';
import { Ranking } from '../ranking';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  ranking : Array<any>;
  rank : Ranking;

  constructor(private rankingService: RankingService) { }

  ngOnInit() {

    this.listar();

  }

  listar(){
    this.rankingService.listar().subscribe(dados => this.ranking = dados);
  }
}
