import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { RankingComponent } from './ranking/ranking.component';
import { RankingService } from './ranking.service';
import { RankingFormComponent } from './ranking-form/ranking-form.component';
import { RankingSelectComponent } from './ranking-select/ranking-select.component'

@NgModule({
  declarations: [
    AppComponent,
    RankingComponent,
    RankingFormComponent,
    RankingSelectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [ RankingService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
